<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="./css/employee.css" rel="stylesheet" />

</head>
<body>


	<h2>REIMBURSEMENT LIST</h2>
	<div class="table-wrapper">
		<table class="fl-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Amount</th>
					<th>Description</th>
					<th>Author</th>
					<th>Status</th>
					<th>Types</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${reimbList }">
					<tr>
						<td><c:out value="${item.id}" /></td>
						<td><c:out value="${item.amount}" /></td>
						<td><c:out value="${item.description}" /></td>
						<td><c:out value="${item.author}" /></td>
						<td><c:out value="${item.statusId}" /></td>
						<td><c:out value="${item.typeId}" /></td>
					</tr>
				</c:forEach>
			<tbody>
		</table>
	</div>
	<div>
		<form class="form" method="post" action="/projectone/reimbursementsubmit.change">
			<p class="white">Add reimbursement to submit</p>
			<input placeholder="Enter amount" name="amount"> <input
				placeholder="Enter description" name="description">  <input
				placeholder="Enter status" name="status"> <input
				placeholder="Enter type" name="type"> <input class="reimbsub" type="submit"
				value="Submit">
		</form>
	</div>
</body>
</html>