<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="./css/manager.css" rel="stylesheet" />

</head>
<body>


	<h2>REIMBURSEMENT LIST</h2>
	<div class="table-wrapper">
		<table class="fl-table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Amount</th>
					<th>Description</th>
					<th>Author</th>
					<th>Status</th>
					<th>Types</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${reimbList }">
					<tr>
						<td><c:out value="${item.id}" /></td>
						<td><c:out value="${item.amount}" /></td>
						<td><c:out value="${item.description}" /></td>
						<td><c:out value="${item.author}" /></td>
						<td><c:out value="${item.statusId}" /></td>
						<td><c:out value="${item.typeId}" /></td>
						<td><a href="<c:url value="/approve.change?id=${item.id}" />">Approve</a></td>
						<td><a href="<c:url value="/deny.change?id=${item.id}" />">Deny</a></td>
					</tr>
				</c:forEach>
			<tbody>
		</table>
	</div>
</body>
</html>