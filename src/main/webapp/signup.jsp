<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="./css/signup.css" rel="stylesheet" />
<script src="./js/signup.js"></script>
</head>
<body>
	
	


	<h2></h2>
	<div class="form" method="post" action="/projectone/signup.change">
		<div class="title">Welcome</div>
		<div class="subtitle">Let's create your account!</div>
		<div class="input-container ic1">
			<input name="firstname" class="input" type="text" placeholder=" " />
			<div class="cut"></div>
			<label for="firstname" class="placeholder">First name</label>
		</div>
		<div class="input-container ic2">
			<input name="lastname" class="input" type="text" placeholder=" " />
			<div class="cut"></div>
			<label for="lastname" class="placeholder">Last name</label>
		</div>
		<div class="input-container ic2">
			<input name="username" class="input" type="text" placeholder=" " />
			<div class="cut cut-short"></div>
			<label for="username" class="placeholder">Username</> 
		</div>
		<div class="input-container ic2">
			<input name="password" class="input" type="text" placeholder=" " />
			<div class="cut cut-short"></div>
			<label for="password" class="placeholder">Password</> 
		</div>
		<div class="input-container ic2">
			<input name="email" class="input" type="text" placeholder=" " />
			<div class="cut cut-short"></div>
			<label for="email" class="placeholder">Email</> 
		</div>
		<div class="input-container ic2">
			<input name="role" class="input" type="text" placeholder=" " />
			<div class="cut cut-short"></div>
			<label for="role" class="placeholder">Role</> 
		</div>
		<form method="get" action="/projectone/indirserv">
		<button method="get" action="/projectone/indirserv" 
			type="submit" class="submit" value="Indirect Servlet GET">submit</button>
		</form>
	</div>
</body>
</html>

