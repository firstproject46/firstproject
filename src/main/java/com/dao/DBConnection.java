package com.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
//	private final LogHelper log = new LogHelper();
	
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream is;
	Properties p = new Properties();
//	
	public DBConnection() {
		is = classLoader.getResourceAsStream("connection.properties");
		try {
			p.load(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public Connection getDBConnection() throws SQLException {
		final String URL = p.getProperty("DATABASE_URL");
		final String USERNAME = p.getProperty("DATABASE_USERNAME");
		final String PASSWORD = p.getProperty("DATABASE_PASSWORD");
        try {
            Class.forName("org.postgresql.Driver");
        } catch(ClassNotFoundException e) {
//        	log.callFatalLogger(e);
        	e.printStackTrace();
        }
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
		

	}
	

}

//
//public class MagicDBConnection {
//
//	// This utility class, has the sole purpose of creating connections to the data base, this will help with eliminating repeated code
//	//it also will help make our code more testable.
//	
//	ClassLoader classLoader = getClass().getClassLoader();
//	InputStream is;
//	Properties p = new Properties();
//	
//	public MagicDBConnection() {
//		is = classLoader.getResourceAsStream("connection.properties");
//		try {
//			p.load(is);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}	
//	public Connection getDBConnection() throws SQLException {
//		final String URL = p.getProperty("url");
//		final String USERNAME = p.getProperty("username");
//		final String PASSWORD = p.getProperty("password");
//		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
//	}
//	
//}




