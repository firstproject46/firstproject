package com.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.example.Reimbursement;
import com.example.User;

public class ReimbDaoImpl implements ReimbDao {
	private final DBConnection db;

	public ReimbDaoImpl(DBConnection db) {
		this.db = db;
	}

	public ReimbDaoImpl() {
		this.db = new DBConnection();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Reimbursement> getAllTickets(Reimbursement reimbursement) {
		List<Reimbursement> reimbursementList = null;

		try (Connection con = db.getDBConnection()) {

			reimbursementList = new ArrayList<>();
			String sql = "select reimb_amount, reimb_description, reimb_author, reimb_status_id, reimb_type_id "
					+ "from ers_reimbursement where reimb_author = ?";
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setInt(1, user.getUid());
			ResultSet rs = prepare.executeQuery();

			while (rs.next()) {
				float amount = rs.getFloat(1);
				String description = rs.getString(2);
				int author = rs.getInt(3);
				int statusId = rs.getInt(4);
				int typeId = rs.getInt(5);

				reimbursementList.add(new Reimbursement(amount, description, author, statusId, typeId));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimbursementList;
	}
	
	@Override
	public List<Reimbursement> getAllReimbursements() {
		List<Reimbursement> reimbList = new ArrayList<>();

		try (Connection con = db.getDBConnection()) {
			String sql = "select reimb_id, reimb_amount, reimb_description, reimb_author, reimb_status_id, reimb_type_id from ers_reimbursement";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt(1);
				float amount = rs.getFloat(2);
				String description = rs.getString(3);
				int author = rs.getInt(4);
				int statusId = rs.getInt(5);
				int typeId = rs.getInt(6);

				reimbList.add(new Reimbursement(id, amount, description, author, statusId, typeId));
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return reimbList;
	}
	
	@Override
	public List<Reimbursement> getReimbursementByAuthorId(int author) {
		List<Reimbursement> reimbList = new ArrayList<>();

		try (Connection con = db.getDBConnection()) {
			String sql = "select reimb_amount, reimb_description, reimb_author, reimb_status_id, reimb_type_id "
					+ "from ers_reimbursement where reimb_author = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, author);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				float amount = rs.getFloat(1);
				String description = rs.getString(2);
				int authorr = rs.getInt(3);
				int statusId = rs.getInt(4);
				int typeId = rs.getInt(5);

				reimbList.add(new Reimbursement(amount, description, authorr, statusId, typeId));
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return reimbList;
	}

	@Override
	public Reimbursement insertReimbursement(float amount, String description, int author, int statusId, int typeId) {
		// TODO Auto-generated method stub
		try (Connection con = db.getDBConnection()) {
			String sql = "insert into ers_reimbursement(reimb_amount, reimb_description, reimb_author, reimb_status_id, reimb_type_id) values(?,?,?,?,?)";

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setFloat(1, amount);
			prepare.setString(2, description);
			prepare.setInt(3, author);
			prepare.setInt(4, statusId);
			prepare.setInt(5, typeId);


			int changed = prepare.executeUpdate();
			prepare.clearBatch();
			System.out.println("num of rows changed: " + changed);


			// create reimbursement
			Reimbursement newReimbursement = new Reimbursement(amount, description, author, statusId, typeId);
			return newReimbursement;

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return null;
		
	}

	@Override
	public Reimbursement getReimbursementById(int id) {
		// TODO Auto-generated method stub
		Reimbursement reimbursement = new Reimbursement();
		
		try(Connection con = db.getDBConnection()) {
			String sql = "select * from ers_reimbursement re where re.reimb_id = ?";
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				reimbursement = new Reimbursement(result.getInt(1), result.getFloat(2), result.getString(3), result.getInt(4), result.getInt(5), result.getInt(6));
			}
		} catch (SQLException e) {

			return null;
		}
		return reimbursement;
	}

	@Override
	public Reimbursement updateReimbursement(Reimbursement reimbursement) {
		Reimbursement newReimbursement = new Reimbursement();
		
		try(Connection con = db.getDBConnection()) {
			String sql = "{? = call update_reimbursement(?,?,?,?,?)}";
			CallableStatement reimb = con.prepareCall(sql);
			reimb.registerOutParameter(1, Types.INTEGER);
			reimb.setInt(2, reimbursement.getId());
			reimb.setDouble(3, reimbursement.getAmount());
			reimb.setString(4, reimbursement.getDescription());
			reimb.setInt(5, reimbursement.getAuthor());
			reimb.setInt(6, reimbursement.getStatusId());
			reimb.setInt(7, reimbursement.getTypeId());
			
			reimb.execute();
			
			newReimbursement = getById(reimbursement.getInt(1));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return newReimbursement;
	}


	@Override
	public List<Reimbursement> getAllTickets(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Reimbursement> getReimbursementByUser(int uid) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void insertReimbursementObject(Reimbursement reimbursement) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Reimbursement> getReimbursementByUserId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Reimbursement> getReimbursementById() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Reimbursement updateReimbursement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Reimbursement> getReimbursementByAuthor(int author) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void approveReimbursement(int id) {
		// TODO Auto-generated method stub
		try(Connection con = db.getDBConnection()) {
			String sql = "update ers_reimbursement set reimb_status_id = 2 where reimb_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void denyReimbursement(int id) {
		// TODO Auto-generated method stub
		try(Connection con = db.getDBConnection()) {
			String sql = "update ers_reimbursement set reimb_status_id = 3 where reimb_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
