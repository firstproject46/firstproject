package com.dao;
import java.util.List;
import java.util.Optional;

import com.example.Reimbursement;
import com.example.User;

public interface Dao {
	
	User insertUser(String userName, String password, String firstName, String lastName, String email, int roleId);
	void insertUserObject(User user);
//	void insertReimbursement(float amount, String description, int author, int statusId, int typeId);
//	void insertReimbursementObject(Reimbursement reimbursement);
	List<User> getAllUsers();
	List<Reimbursement> getAllReimbursements();
	Optional<User> loginUser(User user);
	List<Reimbursement> getAllTickets(User user);
	User getUsername(String username);
	User getUserByName(String username);
	Optional<User> loginUser(String username, String password);


}
