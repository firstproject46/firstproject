package com.dao;
import java.util.List;

import com.example.Reimbursement;
import com.example.User;

public interface ReimbDao {

//	void insertUser(String userName, String password, String firstName, String lastName, String email, int roleId);
//	void insertUserObject(User user);
	Reimbursement insertReimbursement(float amount, String description, int author, int statusId, int typeId);
	void insertReimbursementObject(Reimbursement reimbursement);
//	List<User> getAllUsers();
	List<Reimbursement> getAllReimbursements();
	List<Reimbursement> getAllTickets(User user);
	List<Reimbursement> getAllTickets(Reimbursement reimbursement);
	List<Reimbursement> getReimbursementByUser(int uid);
	List<Reimbursement> getReimbursementByUserId();
	List<Reimbursement> getReimbursementById();
//	Reimbursement getReimbursementByAuthorId(int author);
//	Reimbursement getReimbursementByAuthor(int author);
	List<Reimbursement> getReimbursementByAuthor(int author);
	Reimbursement updateReimbursement(Reimbursement reimbursement);
	Reimbursement updateReimbursement();
	List<Reimbursement> getReimbursementByAuthorId(int author);
	Reimbursement getReimbursementById(int id);
	void approveReimbursement(int id);
	void denyReimbursement(int id);
}
