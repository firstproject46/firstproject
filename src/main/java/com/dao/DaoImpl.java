package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.Reimbursement;
import com.example.User;

public class DaoImpl implements Dao {
	private final DBConnection db;

	public DaoImpl(DBConnection db) {
		this.db = db;
	}

	public DaoImpl() {
		this.db = new DBConnection();
		// TODO Auto-generated constructor stub
	}

	@Override
	public User insertUser(String userName, String password, String firstName, String lastName, String email,
			int roleId) {
		// TODO Auto-generated method stub
		try (Connection con = db.getDBConnection()) {
			String sql = "insert into ers_users(ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) values(?,?,?,?,?,?)";

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);
			prepare.setString(2, password);
			prepare.setString(3, firstName);
			prepare.setString(4, lastName);
			prepare.setString(5, email);
			prepare.setInt(6, roleId);

			int changed = prepare.executeUpdate();
			prepare.clearBatch();
			System.out.println("num of rows changed: " + changed);

			sql = "select * from ers_users where ers_username = ?";
			prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);
			ResultSet rs = prepare.executeQuery();
			rs.next();
			int id = rs.getInt("ers_users_id");
			System.out.println(rs.getInt("ers_users_id"));

			// create user
			User newUser = new User(id, userName, password, firstName, lastName, email, roleId);
			return newUser;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public List<User> getAllUsers() {
		List<User> userList = new ArrayList<>();

		try (Connection con = db.getDBConnection()) {

			// replace * w/ column name
			String sql = "select * from ers_users";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt(1);
				String username = rs.getString(2);
				String password = rs.getString(3);
				String firstname = rs.getString(4);
				String lastname = rs.getString(5);
				String email = rs.getString(6);
				int userrole = rs.getInt(7);

				userList.add(new User(id, username, password, firstname, lastname, email, userrole));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userList;
	}

	@Override
	public Optional<User> loginUser(String username, String password) {

		try (Connection con = db.getDBConnection()) {

//			String sql = "select * from ers_users " + "where ers_username = ?";
			String sql = "select * from ers_users " + "where ers_username = ? and ers_password = ?";


			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, username);
			prepare.setString(2, password);
			ResultSet rs = prepare.executeQuery();

			while (rs.next()) {
				int id = rs.getInt(1);
				String usernamee = rs.getString(2);
				String passwordd = rs.getString(3);
				String firstname = rs.getString(4);
				String lastname = rs.getString(5);
				String email = rs.getString(6);
				int userrole = rs.getInt(7);

				return Optional.of(new User(id, usernamee, passwordd, firstname, lastname, email, userrole));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
	
	@Override
	public List<Reimbursement> getAllTickets(User user) {
		List<Reimbursement> reimbursementList = null;

		try (Connection con = db.getDBConnection()) {

			reimbursementList = new ArrayList<>();
			// replace * w/ column name
			// implied join + columns need to return
//			String sql = "select a.reimb_amount, b.reimb_status from ers_reimbursment a, ers_reimbursement_status b,"
//					+ "c.ers_reimbursement_type c where a.reimb_status_id = b.reimb_status_id & a.reimb_type_id = c.reimb_type_id & a.reimb_author = ?";
			String sql = "select reimb_amount, reimb_description, reimb_author, reimb_status_id, reimb_type_id "
					+ "from ers_reimbursement where reimb_author = ?";
			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setInt(1, user.getUid());
			ResultSet rs = prepare.executeQuery();

			while (rs.next()) {
				float amount = rs.getFloat(1);
				String description = rs.getString(2);
				int author = rs.getInt(3);
				int statusId = rs.getInt(4);
				int typeId = rs.getInt(5);

				reimbursementList.add(new Reimbursement(amount, description, author, statusId, typeId));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimbursementList;
	}

	@Override
	public List<Reimbursement> getAllReimbursements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertUserObject(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User getUserByName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<User> loginUserNew(String username, String password) {
		// TODO Auto-generated method stub
		return null;
	}

}


