package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.dao.DaoImpl;
import com.dao.ReimbDaoImpl;
import com.example.Reimbursement;
import com.example.User;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

public class ReimbController {
	
	static UserService userService = new UserService(new DaoImpl(), new ReimbDaoImpl());
	static ReimbursementService reimbService = new ReimbursementService(new DaoImpl(), new ReimbDaoImpl());
	
	public static String login(HttpServletRequest req) {
		System.out.println("in reimbursement controller login");
		if(!req.getMethod().equals("POST")) {
			return "html/unsuccessfullogin.html";
		}
		
		//next process out info that is sent in the request
		User user = userService.getUserVerifyLogin(req.getParameter("username"), req.getParameter("password"));

		if (user == null) {
			return "invalidcreds.change";
			
		} else {
			req.getSession().setAttribute("currentUser", user);
			return "html/home.html";
		}
	}
	
//	public static List<Reimbursement> getReimbursementByAuthorId(int author){
//		return userService.getReimbursementforUser(author);
//	}
	
	public List<Reimbursement> getReimbursementByUserId(HttpServletRequest req) {
		System.out.println("in reimbursement controller reimbursement list for user");
		return null;

	}
	
	public List<Reimbursement> getReimbursementAll (HttpServletRequest req) {
		System.out.println("in reimbursement controller get all user reimbursement");
		return null;
	}
	
	public Reimbursement updateReimbursement (HttpServletRequest req) {
		System.out.println("in reimbursement controller update reimbursement--manager only");
		return null;
	}
}
