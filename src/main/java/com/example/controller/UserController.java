package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.dao.DaoImpl;
import com.dao.ReimbDaoImpl;
import com.example.Reimbursement;
import com.example.User;
import com.example.service.UserService;

public class UserController {
	
	static UserService userService = new UserService(new DaoImpl(), new ReimbDaoImpl());
//	static UserService userService = new UserService();

//		HttpSession session = 
	
	public static String login(HttpServletRequest req) {
		System.out.println("in user controller login");
		if(!req.getMethod().equals("POST")) {
			return "html/unsuccessfullogin.html";
		}
		
		//next process out info that is sent in the request
		User user = userService.getUserVerifyLogin(req.getParameter("username"), req.getParameter("password"));

		if (user == null) {
			return "html/unsuccessfullogin.html";
		} else if (user.uid == 1){
			req.getSession().setAttribute("currentUser", user);
			List<Reimbursement> reimbList = UserController.getReimbursementByAuthorId(user.getUid());
			req.setAttribute("reimbList", reimbList);
//			return "html/employee.html";
			return "employee.jsp";
		} else {
			List<Reimbursement> reimbList = UserController.getAllReimbursements(user.getUid());
			req.getSession().setAttribute("currentUser", user);
			req.setAttribute("reimbList", reimbList);
			return "manager.jsp";
//			return "html/manager.html";
		}
	}
	
	public static List<Reimbursement> getReimbursementByAuthorId(int author){
		return userService.getReimbursementforUser(author);
	}
	
	public static List<Reimbursement> getAllReimbursements(int author){
		return userService.getAllReimbursements();
	}
	
	public static String insertReimbursement(HttpServletRequest req) {
		User user = (User)req.getSession().getAttribute("currentUser");
		float amount = Float.parseFloat(req.getParameter("amount")); 
		String description = req.getParameter("description"); 
		int author = user.getUid();
		int statusId = Integer.parseInt(req.getParameter("status")); 
		int typeId = Integer.parseInt(req.getParameter("type"));
		
		
		userService.insertReimbursementUser(amount, description, author, statusId, typeId);
		List<Reimbursement> reimbList = UserController.getReimbursementByAuthorId(user.getUid());
		req.setAttribute("reimbList", reimbList);
//		return "html/employee.html";
		return "employee.jsp";
	}
	
	public static String approveReimbursement(HttpServletRequest req, int id) {
		userService.approveReimbursement(id);
		User user = (User)req.getSession().getAttribute("currentUser");
		List<Reimbursement> reimbList = UserController.getAllReimbursements(user.getUid());
		req.setAttribute("reimbList", reimbList);
		return "manager.jsp";
	}
	
	public static String denyReimbursement(HttpServletRequest req, int id) {
		userService.denyReimbursement(id);
		User user = (User)req.getSession().getAttribute("currentUser");
		List<Reimbursement> reimbList = UserController.getAllReimbursements(user.getUid());
		req.setAttribute("reimbList", reimbList);
		return "manager.jsp";
	}
	
	public static String insertUser(HttpServletRequest req) {
		

		User user = userService.getUserVerifyLogin(req.getParameter("username"), req.getParameter("password"));

		if (user == null) {
			return "signup.jsp";
		}
		return null;
	
	
	
	}
	

}
	


