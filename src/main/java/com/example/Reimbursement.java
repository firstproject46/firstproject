package com.example;

public class Reimbursement {
	private int id;
	private float amount;
	private String description;
//	private byte[] receipt;
	private int author;
	private int statusId;
	private int typeId;

	public Reimbursement() {
		// TODO Auto-generated constructor stub
	}

	public Reimbursement(int id, float amount, String description, int author, int statusId, int typeId) {
		super();
		this.id = id;
		this.amount = amount;
		this.description = description;
		this.author = author;
		this.statusId = statusId;
		this.typeId = typeId;
	}

	public Reimbursement(float amount, String description, int author, int statusId, int typeId) {
		super();
		this.amount = amount;
		this.description = description;
		this.author = author;
		this.statusId = statusId;
		this.typeId = typeId;
	}
	
	public Reimbursement(int id) {
		super();
		this.id = id;

	}

	@Override
	public String toString() {
		return "Reimbursement [id=" + id + ", amount=" + amount + ", description=" + description + ", author=" + author
				+ ", statusId=" + statusId + ", typeId=" + typeId + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAuthor() {
		return author;
	}

	public void setAuthor(int author) {
		this.author = author;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

}
