package com.example.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.Reimbursement;
import com.example.User;
import com.example.controller.UserController;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ViewServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		req.getRequestDispatcher(ViewDispatcher.process(req, res)).forward(req, res);
//		res.sendRedirect("html/secondpage.html");
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
//		req.getRequestDispatcher(ViewDispatcher.process(req, res)).forward(req, res);
		
		
		req.getRequestDispatcher(ViewDispatcher.process(req, res)).forward(req, res);


		
	}

}
