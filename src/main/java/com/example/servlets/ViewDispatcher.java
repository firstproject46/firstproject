package com.example.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.controller.ReimbController;
import com.example.controller.UserController;

public class ViewDispatcher { // called by the view servlet, filters the URI of the req passed to it
	// determines which controller it should call and return string that will change
	// view of user

	private UserController uc = new UserController();
	private ReimbController rc = new ReimbController();

	public static String process(HttpServletRequest req, HttpServletResponse res) throws IOException {
		switch (req.getRequestURI()) {

		case "/projectone/login.change":
			System.out.println("in login.change dispatcher");
			return UserController.login(req);
//				String as = UserController.login(req);
//				PrintWriter pw = res.getWriter();
//				pw.println("<h1> Hello"+req.getSession().getAttribute("currentUser")+"<h1>");
//				return as;
//		case "/projectone/signup":
//			System.out.println("in signup.change dispatcher");
//			return UserController.signup(req);
		case "/projectone/reimbursementsubmit.change":
			System.out.println("in reimbursement dispatcher");
			return UserController.insertReimbursement(req);
		case "/projectone/approve.change":
			System.out.println("in approve reimb dispatcher");
			int id = Integer.parseInt(req.getParameter("id"));
			return UserController.approveReimbursement(req, id);
		case "/projectone/deny.change":
			System.out.println("in deny reimb dispatcher");
			int id1 = Integer.parseInt(req.getParameter("id"));
			return UserController.denyReimbursement(req, id1);
		case "/projectone/signup.change":
			System.out.println("in signup reimb dispatcher");
			return UserController.insertUser(req);
		default:
			System.out.println("in default");
			return "html/unsuccessfullogin.html";
		}
	}
}

//return UserController.getReimbursementByAuthorId(req);