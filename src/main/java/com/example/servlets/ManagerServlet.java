package com.example.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.User;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ManagerServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		System.out.println("inside direct servlet, doGet");

		PrintWriter printOut = res.getWriter();
		// this is what gets sent when clicked in index html
		printOut.print("<html><body><h1>Servlet directly responding</h1></body></html>");
	}

	@Override
	protected void doPost(HttpServletRequest rep, HttpServletResponse res) throws IOException, ServletException {
		System.out.println("inside direct servlet doPost");

		User user = new User(2, "parsley", "password", "password", "parsley", "password", 1);

//		res.getWriter().write(new ObjectMapper().writeValueAsString("look"));
		res.getWriter().write(new ObjectMapper().writeValueAsString(user));
	}

}
