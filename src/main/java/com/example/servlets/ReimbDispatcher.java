package com.example.servlets;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.Reimbursement;
import com.example.controller.ReimbController;

public class ReimbDispatcher {
	private ReimbController rc = new ReimbController();
	
	public List<Reimbursement> process(HttpServletRequest req) {
		switch(req.getRequestURI()) {
			case "/project1/reimbursementuser.change":
				System.out.println("in request for user id reimbursement");
				return rc.getReimbursementByUserId(req);
			case "/project1/reimbursementmanager.change":
				System.out.println("in request for get all reimbursement");
				return rc.getReimbursementAll(req);
			case "/project1/reimbursementmanageredit.change":
				System.out.println("in request for get all reimbursement");
				return rc.getReimbursementAll(req);
			default:
				System.out.println("in default");
				return null;
		}
	}

}
