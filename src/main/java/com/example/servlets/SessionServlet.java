package com.example.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.User;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SessionServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		System.out.println("in session servlet GET");
		
		User user = new User("testparsley", "password");
		
		//Session code
		HttpSession session = req.getSession();
		req.getParameterMap().get("username");
		System.out.println(req.getParameterMap().get("username"));
		session.setAttribute("currentUser", user);
		res.sendRedirect("html/index.html");
		
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		System.out.println("in session servlet POST");
		
		//access the session information in this method (getting values form session above)
		HttpSession session = req.getSession();
		User sessionUser = (User)session.getAttribute("currentUser");
		res.getWriter().write(new ObjectMapper().writeValueAsString(sessionUser));
	}

}
