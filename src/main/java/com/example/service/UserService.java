package com.example.service;

import java.util.List;
import java.util.Optional;

import com.dao.DaoImpl;
import com.dao.ReimbDao;
import com.example.Reimbursement;
import com.example.User;

public class UserService {
	private DaoImpl cDao;
	private ReimbDao rDao;

	public UserService() {
		// TODO Auto-generated constructor stub
	}

	public UserService(DaoImpl cDao, ReimbDao rDao) {
		super();
		this.cDao = cDao;
		this.rDao = rDao;
	}
	
	
	public User getUserVerifyLogin(String username, String password) {
		return  cDao.loginUser(username, password).orElse(null);
//		User user = cDao.loginUser(username, password).get();
//		if(user != null) {
//			if(user.getPassword().equals(password)) {
//				return user;
//			}
//		}
		
	}
	
	
	
	public List<Reimbursement> getReimbursementforUser(int uid){
		List<Reimbursement> reimbList = rDao.getReimbursementByAuthorId(uid);
//		if (reimbList != null) {
//			System.out.println("no values yet");
//		}
		return reimbList;
		
	}
	
	
	public List<Reimbursement> getAllReimbursements(){
		List<Reimbursement> reimbList = rDao.getAllReimbursements();
//		if (reimbList != null) {
//			System.out.println("no values yet");
//		}
		return reimbList;
		
	}
	
	public Reimbursement insertReimbursementUser(float amount, String description, int author, int statusId, int typeId) {
		return rDao.insertReimbursement(amount, description, author, statusId, typeId);
	}
	public void approveReimbursement(int id) {
		rDao.approveReimbursement(id);
	}
	
	public void denyReimbursement(int id) {
		rDao.denyReimbursement(id);
	}
	
	public User insertUser(String username, String password, String firstName, String lastName, String email, int roleId) {
		return cDao.insertUser(username, password, firstName, lastName, email, roleId);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}



