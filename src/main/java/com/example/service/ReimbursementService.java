package com.example.service;

import java.util.List;

import com.dao.DaoImpl;
import com.dao.ReimbDao;
import com.example.Reimbursement;

public class ReimbursementService {
	private DaoImpl cDao;
	private ReimbDao rDao;

	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(DaoImpl cDao, ReimbDao rDao) {
		super();
		this.cDao = cDao;
		this.rDao = rDao;
	}

	public List<Reimbursement> getAllReimbursements() {
		List<Reimbursement> reimbList = rDao.getAllReimbursements();
//	if (reimbList != null) {
//		System.out.println("no values yet");
//	}
		return reimbList;

	}
	
	public Reimbursement insertReimb(float amount, String description, int author, int statusId, int typeId) {
		
		Reimbursement newReimb = rDao.insertReimbursement(amount, description, author, statusId, typeId);
		return newReimb;
	}
	
	public Reimbursement updateReimb(Reimbursement reimbursement) {
		
			if (rDao.getReimbursementById(reimbursement.getId()) != null) {
				Reimbursement updatedReimbursement = rDao.updateReimbursement(reimbursement);
				return updatedReimbursement;
			}
		return null;
	}
	
	public List<Reimbursement> getReimbursementforUser(int uid){
		List<Reimbursement> reimbList = rDao.getReimbursementByAuthorId(uid);
//		if (reimbList != null) {
//			System.out.println("no values yet");
//		}
		return reimbList;
		
	}

}
