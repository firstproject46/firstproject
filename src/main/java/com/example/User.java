package com.example;

import java.sql.ResultSet;

public class User {
//	private final int uid;
//	private final String username;
//	private final String password;
//	private final String firstName;
//	private final String lastName;
//	private final String email;
//	private final int roleId;
	public int uid;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int roleId;
	
//	public static void main(String[] args) {
//		
//	}
	
	public User(int uid, String username, String password, String firstName, String lastName, String email, int roleId) {
		super();
		this.uid = uid;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
	}
		
//	public User() {
//		// TODO Auto-generated constructor stub
//	}
//	
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public void setUid(int uid) {
		this.uid = uid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public User(String username) {
		this.username = username;
	}
	// remove setters for user object

	public int getUid() {
		return uid;
	}

//	public void setUid(int uid) {
//		this.uid = uid;
//	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", username=" + username + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", roleId=" + roleId + "]";
	}

	public String getUsername() {
		return username;
	}

//	public void setUsername(String username) {
//		this.username = username;
//	}

	public String getPassword() {
		return password;
	}

//	public void setPassword(String password) {
//		this.password = password;
//	}

	public String getFirstName() {
		return firstName;
	}

//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}

	public String getLastName() {
		return lastName;
	}

//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}

	public String getEmail() {
		return email;
	}

//	public void setEmail(String email) {
//		this.email = email;
//	}

	public int getRoleId() {
		return roleId;
	}

//	public void setRoleId(int roleId) {
//		this.roleId = roleId;
//	}


	
	
	
}
