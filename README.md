EMPLOYEE REIMBURSEMENT SYSTEM (ERS)

* An employee reimbursement system that allows users(employees or administrators) to log in and handle reimbursements.

* An employee can view all of their own (current/past/pending) reimbursements and submit additional reimbursements.
* An administrator can view all employee reimbursements and filter through status as well as approve or deny any reimbursement.

* Technologies used include Java, PostgresSQL, Maven, Tomcat, AWS, and JDBC.


CLONE: https://gitlab.com/firstproject46/firstproject.git
